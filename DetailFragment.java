package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private DetailViewModel viewModel;
    private DetailFragmentArgs args;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book
        args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.findBook((int)args.getBookNum());

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        observerSetup();
        listenerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    private void listenerSetup() {
        Button updateButton = getView().findViewById(R.id.buttonUpdate);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = textTitle.getText().toString();
                String authors = textAuthors.getText().toString();
                String year = textYear.getText().toString();
                String genre = textGenres.getText().toString();
                String publisher = textPublisher.getText().toString();
                Book book;
                if((int)args.getBookNum() != -1)
                    book = viewModel.getSelectedBook().getValue();
                else
                    book = new Book(title, authors, year, genre, publisher);
                book.setTitle(title);
                book.setAuthors(authors);
                book.setYear(year);
                book.setGenres(genre);
                book.setPublisher(publisher);
                viewModel.insertOrUpdateBook(book);
            }
        });
    }

    private void observerSetup() {
        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(Book book) {
                        if(book != null) {
                            textTitle.setText(book.getTitle());
                            textAuthors.setText(book.getAuthors());
                            textYear.setText(book.getYear());
                            textGenres.setText(book.getGenres());
                            textPublisher.setText(book.getPublisher());
                        }else{
                            textTitle.setText("");
                            textAuthors.setText("");
                            textYear.setText("");
                            textGenres.setText("");
                            textPublisher.setText("");
                        }
                    }
                });
    }
}