package fr.uavignon.ceri.tp2;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import fr.uavignon.ceri.tp2.data.Book;

import java.util.List;

@Dao
public interface BookDao {

    @Update
    void updateBook(Book book);

    @Insert
    void insertBook(Book book);

    @Query("SELECT * FROM books WHERE booksId = :id")
    Book getBook(long id);

    @Query("DELETE FROM books WHERE booksId = :id")
    void deleteBook(long id);

    @Query("SELECT * FROM books")
    LiveData<List<Book>> getAllBooks();
}
