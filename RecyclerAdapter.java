package fr.uavignon.ceri.tp2;


import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {

    private ActionMode actionMode;
    private FragmentActivity activity;
    private long position;
    ListViewModel viewModel;


    public RecyclerAdapter(FragmentActivity activity,ListViewModel viewModel) {
        this.activity = activity;
        this.viewModel = viewModel;
    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.deleteButton:
                    System.out.println(viewModel.getAllBooks().getValue());
                    viewModel.deleteBook(position);
                    System.out.println(viewModel.getAllBooks().getValue());
                    System.out.println(position);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
       public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };

    private static final String TAG = fr.uavignon.ceri.tp2.RecyclerAdapter.class.getSimpleName();
    private List<Book> bookList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(bookList.get(i).getTitle());
        viewHolder.itemDetail.setText(bookList.get(i).getAuthors());

    }

    @Override
    public int getItemCount() {
        return bookList == null ? 0 : bookList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;
        ConstraintLayout constraintLayout;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);
            constraintLayout = itemView.findViewById(R.id.relativeLayout);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    /*
                        Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                     */

                    long id = RecyclerAdapter.this.bookList.get((int)getAdapterPosition()).getId() ;
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();

                    action.setBookNum(id);
                    Navigation.findNavController(v).navigate(action);


                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (actionMode != null) {
                        return false;
                    }
                    position = (long)getAdapterPosition();
                    actionMode = activity.startActionMode(actionModeCallback);
                    view.setSelected(true);
                    return true;
                }
            });

        }
    }

    public void setBookList(List<Book> books){
        bookList = books;
        notifyDataSetChanged();
    }

}