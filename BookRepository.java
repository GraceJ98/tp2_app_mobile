package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import fr.uavignon.ceri.tp2.BookDao;
import fr.uavignon.ceri.tp2.BookRoomDatabase;
import fr.uavignon.ceri.tp2.data.Book;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class BookRepository {

    private MutableLiveData<Book> selectedBook = new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public void insertBook(Book newBook) {
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newBook);
        });
    }

    public void deleteBook(long id) {
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void updateBook(Book book){
        BookRoomDatabase.databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }

    public void getBook(long id) {
        Future<Book> fBook = BookRoomDatabase.databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fBook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
